const { Sequelize } = require('sequelize');

const sequelize = new Sequelize({
    dialect: 'postgres',
    dialectModule: require('pg'),
    host: 'aws-0-eu-central-1.pooler.supabase.com',
    port: 5432,
    username: 'postgres.irbcecklunesuhaodjru',
    password: 'piy51Mm6aa7QVDKf',
    database: 'postgres',
});

/*const sequelize = new Sequelize('postgres://postgres.irbcecklunesuhaodjru:piy51Mm6aa7QVDKf@aws-0-eu-central-1.pooler.supabase.com:5432/postgres');*/

/*const sequelize = new Sequelize('test', 'postgres', 'spd', {
    host: 'localhost',
    dialect: 'postgres',
});*/

sequelize
    .authenticate()
    .then(() => {
        console.log('Connection has been established successfully.');
    })
    .catch(err => {
        console.error('Unable to connect to the database:', err);
    });

const User = sequelize.define('user', {
    id: {
        type: Sequelize.INTEGER,
        primaryKey: true,
        autoIncrement: true
    },
    name: {
        type: Sequelize.STRING
    },
    email: {
        type: Sequelize.STRING
    },
    password: {
        type: Sequelize.STRING
    },
    isAdmin: {
        type: Sequelize.BOOLEAN,
        defaultValue: false
    }
}, {timestamps: false});

const Article = sequelize.define('article', {
    id: {
        type: Sequelize.INTEGER,
        primaryKey: true,
        autoIncrement: true
    },
    title: {
        type: Sequelize.STRING
    },
    description: {
        type: Sequelize.STRING
    },
    content: {
        type: Sequelize.TEXT
    },
    creationDate: {
        type: Sequelize.DATE,
        defaultValue: Sequelize.NOW
    },
    image: {
        type: Sequelize.STRING,
        allowNull: true
    }
});

const Test = sequelize.define('test', {
    id: {
        type: Sequelize.INTEGER,
        primaryKey: true,
        autoIncrement: true
    },
    name: {
        type: Sequelize.STRING
    },
    description: {
        type: Sequelize.STRING
    },
    image: {
        type: Sequelize.STRING,
        allowNull: true
    },
    isConfirmed: {
        type: Sequelize.BOOLEAN,
        defaultValue: false
    }
});

const Question = sequelize.define('question', {
    id: {
        type: Sequelize.INTEGER,
        primaryKey: true,
        autoIncrement: true
    },
    description: {
        type: Sequelize.STRING
    }
});

const Answer = sequelize.define('answer', {
    id: {
        type: Sequelize.INTEGER,
        primaryKey: true,
        autoIncrement: true
    },
    description: {
        type: Sequelize.STRING
    },
    score: {
        type: Sequelize.INTEGER
    }
});

const Result = sequelize.define('result', {
    id: {
        type: Sequelize.INTEGER,
        primaryKey: true,
        autoIncrement: true
    },
    score: {
        type: Sequelize.INTEGER
    },
    description: {
        type: Sequelize.STRING
    }
});

const TestResult = sequelize.define('test_result', {
    id: {
        type: Sequelize.INTEGER,
        primaryKey: true,
        autoIncrement: true
    },
    description: {
        type: Sequelize.STRING
    }
});

const Comment = sequelize.define('comment', {
    id: {
        type: Sequelize.INTEGER,
        primaryKey: true,
        autoIncrement: true
    },
    description: {
        type: Sequelize.STRING
    }
});

Test.hasMany(Question);
Question.belongsTo(Test);

Test.hasMany(Result);
Result.belongsTo(Test);

Question.hasMany(Answer);
Answer.belongsTo(Question);

Article.hasMany(Comment);
Comment.belongsTo(Article);

Comment.belongsTo(User);
User.hasMany(Comment);

User.hasMany(TestResult)
Test.hasMany(TestResult)
TestResult.belongsTo(User)
TestResult.belongsTo(Test)

User.hasMany(Test)
Test.belongsTo(User)

sequelize.sync({ alter: true });

module.exports = {
    User,
    Article,
    Test,
    Question,
    Answer,
    Result,
    TestResult,
    Comment
};

